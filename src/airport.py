from tkinter import E
from exceptions import FlightCrashedException, FlightNotInAirspaceException
from threading import Lock
from typing import Dict, List, Tuple
from airspace import Airspace
from corridor import Corridor


class Airport:
    def __init__(
        self, air_space: Airspace, corridors: List[Corridor], max_capacity: int = 100
    ):
        self.mutex: Lock = Lock()
        self.max_capacity: int = max_capacity
        self.corridors: List[Corridor] = corridors
        self.air_space: Airspace = air_space
        self.crashed_not_reg: Dict[str, str] = dict()

    def get_flight_position(self, flight_number: int):
        try:
            self.mutex.acquire()
            if not self.air_space.flight_in_airspace(flight_number):
                raise FlightNotInAirspaceException

            flight_position = self.air_space.get_flight_curent_position(flight_number)
            return flight_position

        except Exception as e:
            print(e)

        finally:
            if self.mutex.locked():
                self.mutex.release()

    def register_flight(
        self, flight_number: int, flight_position: Tuple[int, int, int]
    ):
        try:
            self.mutex.acquire()
            if not self.air_space.flight_in_airspace(flight_number):
                self.air_space.add_flight(flight_number, flight_position)
            else:
                flight_position = self.get_flight_position(flight_number)

            return flight_position
        except Exception as e:
            print(e)

        finally:
            if self.mutex.locked():
                self.mutex.release()

    def update_flight_position(
        self, flight_number: int, flight_position: Tuple[int, int, int]
    ):
        try:
            self.mutex.acquire()
            if not self.air_space.flight_in_airspace(flight_number):
                self.air_space.remove_flight_from_space(flight_number)
                raise FlightNotInAirspaceException

            if flight_number in self.crashed_not_reg():
                raise FlightCrashedException

            self.air_space.actualize_flight_position(flight_position)
            has_crashed, flight_crashed_in = self.air_space.check_for_incidents(flight_number)
            if has_crashed:
                self.crashed_not_reg[flight_crashed_in] = flight_number
                raise FlightCrashedException

        except Exception as e:
            print(e)

        finally:
            if self.mutex.locked():
                self.mutex.release()

    def remove_flight_from_airspace(self, flight_number: int):
        try:
            self.mutex.acquire()
            if self.air_space.flight_in_airspace(flight_number):
                self.air_space.remove_flight_from_space(flight_number)

        except Exception as e:
            print(e)

        finally:
            if self.mutex.locked():
                self.mutex.release()

    def get_coordinates_from_corridors(self, corridor_enrty_point: tuple = None):
        try:
            self.mutex.acquire()
            if corridor_enrty_point:
                corridor: Corridor = list(
                    filter(
                        (lambda c: c.get_entry_point() == corridor_enrty_point),
                        self.corridors,
                    )
                )[0]
                return corridor.get_touchdown_point()

            corridors: List[Corridor] = list(
                filter(lambda c: c.get_is_occupied() == False), self.corridors
            )[0]
            if corridors:
                corridors[0].set_is_occupied(True)
                return corridors[0].get_entry_point()

        except Exception as e:
            print(e)

        finally:
            if self.mutex.locked():
                self.mutex.release()
