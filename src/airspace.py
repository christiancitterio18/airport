import utils


class Airspace:

    __airtraffic__: dict

    def __init__(self, latitude: tuple, longitude: tuple, max_altitude: int) -> None:
        self.latitude = latitude
        self.longitude = longitude
        self.max_altitude = max_altitude
        self.__airtraffic__ = {}

    def add_flight(self, flight_number: str, current_position: tuple) -> None:
        self.__airtraffic__[flight_number] = [current_position]

    def actualize_flight_position(
        self, flight_number: str, current_position: tuple
    ) -> None:
        self.__airtraffic__[flight_number].append(current_position)

    def get_flight_curent_position(self, flight_number: str) -> tuple:
        return self.__airtraffic__[flight_number][-1]

    def check_for_incidents(self, flight_number: str) -> bool:
        for k, v in self.__airtraffic__.values():
            last_position: tuple = self.__airtraffic__[flight_number][-1]
            if k == flight_number:
                continue
            elif utils.check_crashes(last_position, v[-1]):
                return True, k
            else:
                continue

        return False

    def flight_in_airspace(self, flight_number: str) -> bool:
        return flight_number in self.__airtraffic__.keys()

    def remove_flight_from_space(self, flight_number: str) -> None:
        del self.__airtraffic__[flight_number]
