class FlightCrashedException(Exception):
    pass


class FlightNotInAirspaceException(Exception):
    pass
