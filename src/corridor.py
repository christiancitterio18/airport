class Corridor:

    is_occupied = False

    def __init__(self, latitude: tuple, longitude: tuple) -> None:
        self.latitude = latitude
        self.longitude = longitude

    def get_entry_point(self) -> tuple:

        if self.latitude[1] > 1000:
            return 8500, self.longitude
        else:
            return self.latitude, 8500

    def get_touchdown_point(self) -> tuple:
        return 500, 0, 500

    def set_is_occupied(self, value: bool) -> None:
        self.is_occupied = value

    def get_is_occupied(self) -> bool:
        return self.is_occupied
