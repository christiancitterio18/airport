from typing import Tuple
from utils import create_flight_number


class Flight():

    def __init__(self) -> None:
        self.flight_number: str = create_flight_number()
        self.fuel: int = 1800
        self.position: Tuple[int, int, int] = None

    def get_fligth_number(self) -> str:
        return self.flight_number

    def get_current_position(self) -> Tuple[int, int, int]:
        return self.position

    def get_fuel(self) -> int:
        return self.fuel

    def set_current_position(self, new_position: Tuple[int, int, int]) -> None:
        self.position = new_position

    def set_fuel(self) -> None:
        self.fuel -= 1
