from random import randint
import random
import string
from typing import Any, List


def approach_airspace(airspace_coordinates: tuple):
    """
    Assign random airspace entrypoint for approaching flight.

    Args:
        airspace_coordinates (tuple): airspace coordinates.

    Returns:
        tuple: flight initial position in airspace;
    """

    if randint(0, 1):
        return (randint(9500, 10000), 5000, randint(*list(airspace_coordinates[2])))

    return (randint(*list(airspace_coordinates[0])), 5000, randint(9500, 10000))


def approach_aircorridor(entry_point: tuple, flight_position: tuple) -> tuple:
    """
    Calculate new position for a flight approaching air corridor entrypoint.

    Args:
        entry_point (tuple): entrypoint coordinates;
        flight_position (tuple): current flight position.

    Returns:
        tuple: new flight position.
    """
    e_lat, e_long = entry_point
    f_lat, f_alt, f_long = flight_position

    return calc_lat_long(e_lat, f_lat), f_alt, calc_lat_long(e_long, f_long)


def approach_landing_point(
    touchdown_coordinates: tuple, flight_position: tuple, corridor_number: int
) -> tuple:
    """
    Calculate new position for flight approaching the landing point.

    Args:
        touchdown_coordinates (tuple): coordinateos of the landing point;
        flight_position (tuple): current flight position;
        corridor_number (int): air corridor number.

    Returns:
        tuple: new flight position.
    """
    td_lat, td_long = touchdown_coordinates
    f_lat, f_alt, f_long = flight_position

    if corridor_number > 1:
        f_lat -= 1 if td_lat < f_lat else 0
        return f_lat, land_alt(f_alt), land_lat_long(td_long, f_long)
    else:
        f_long -= 1 if td_long < f_long else 0
        return land_lat_long(td_lat, f_lat), land_alt(f_alt, f_lat), f_long


def wait_for_free_corridor(flight_position: tuple) -> tuple:
    """
    Calculate a new position for a flight in the airspace.

    Args:
        flight_position (tuple): current flight position.

    Returns:
        tuple: new flight position.
    """
    f_lat, f_alt, f_long = flight_position

    return circ_lat_long(f_lat), f_alt, circ_lat_long(f_long)


def circ_lat_long(lat_long_flt: int) -> int:
    """
    Tries to emulate a semicircolar movement on the longitude plane.

    Args:
        lat_long_flt (int): current flight longitude ot latitude

    Returns:
        int: new flight longitude or latitude
    """
    add_sub: int = 0

    if lat_long_flt == 4000:
        return lat_long_flt

    if lat_long_flt in range(3500, 4501):
        add_sub = 1
    elif lat_long_flt in range(1500, 2001):
        add_sub = 3
    else:
        add_sub = 2

    if lat_long_flt < 4000:
        return lat_long_flt + add_sub

    return lat_long_flt - add_sub


def calc_lat_long(lat_long_range: tuple, lat_long_flt: int) -> int:
    """
    Calculate latitude or longitude for flight approaching air corridor.
    (it calculate for air corridor widht)

    Args:
        lat_long_range (tuple): air corridor width at entrypoint;
        lat_long_flt (int): current flight latitude or longitude.

    Returns:
        int: new flight latitude or longitude
    """

    max: int = lat_long_range[1]
    med: int = int((lat_long_range[1] + lat_long_range[0]) / 2)
    add_sub: int = 1

    if lat_long_flt > max + 500:
        add_sub = 2
    if lat_long_flt == med:
        return lat_long_flt

    return lat_long_flt - add_sub if lat_long_flt > med else lat_long_flt + add_sub


def calc_lat_long(lat_long_point: int, lat_long_flt: int) -> int:
    """
    Calculate latitude or longitude for flight approaching air corridor.
    (it calculate for air corridor lenght)

    Args:
        lat_long_point (int): entrtypoint coordinate of aircorridor;
        lat_long_flt (int): current flight latitude or longitude.

    Returns:
        int: new flight latitude or longitude
    """

    if lat_long_flt in range(lat_long_point, 9001):
        return lat_long_flt
    elif lat_long_flt > 9000:
        return lat_long_flt - 1
    elif lat_long_flt in range(lat_long_point - 500, lat_long_point + 1):
        return lat_long_flt + 2
    else:
        return lat_long_flt + 3


def land_lat_long(lat_long_point: int, lat_long_flt: int, alt_flt: int) -> int:
    """
    Calculate new flight longitude or latitude for air corridor landing point.

    Args:
        lat_long_point (int): longitude or latitude of the landing point;
        lat_long_flt (int): current longitude or latitude of the flight;
        alt_flt (int): current altitude of the flight.

    Returns:
        int: new flight latitude or longitude
    """

    if lat_long_flt <= lat_long_point:
        return lat_long_flt

    if alt_flt > 3000 and lat_long_flt <= 8000:
        return lat_long_flt - 3
    elif lat_long_flt > 1500:
        return lat_long_flt - 2
    else:
        return lat_long_flt - 1


def land_alt(alt_flt: int, lat_long_flt: int) -> int:
    """
    Calculate new altitude for flight approaching landing point.

    Args:
        alt_flt (int): curent flight altitude;
        lat_flt (int): curent flight latitude or longitude.

    Returns:
        int: new flight altitude;
    """

    if lat_long_flt > 8000 or alt_flt == 0:
        return alt_flt

    if alt_flt > 3000:
        return alt_flt - 2
    else:
        return alt_flt - 1


def check_crashes(flight_position: tuple, possible_crash_position: tuple) -> bool:
    """
    Checks for possible crashes with other planes positions.
    A crash is recognized when the distance between 2 planes is <= 10 points

    Args:
        flight_position (tuple): current flight position
        possible_crash_position (tuple): current position of other flight

    Returns:
        bool: if is a crash
    """

    f_lat, f_alt, f_long = flight_position
    crash_lat, crash_alt, crash_long = possible_crash_position

    return not (
        -10 <= f_lat - crash_lat <= 10
        and -10 <= f_alt - crash_alt <= 10
        and -10 <= f_long - crash_long <= 10
    )


def has_landed(flight_position: tuple, landing_point: tuple) -> bool:

    f_lat, f_alt, f_long = flight_position
    land_lat, land_alt, land_long = landing_point

    return (
        f_lat <= land_lat
        and f_alt == land_alt
        and f_long <= land_long
    )


def create_flight_number() -> str:

    def rand_str(characters: Any, number: int):
        return random.choices(characters, number)

    alfa = string.ascii_uppercase
    numeric = string.digits

    return f'{rand_str(numeric, 3)}-{rand_str(alfa, 2)}'
